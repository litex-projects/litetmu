import ltmu_test

def task_convert():
    return {
        "actions": ["python3 ltmu_soc.py basys3 --no-compile"],
        "file_dep": ["ltmu.py", "ltmu_soc.py"],
        "doc": "Only build the verilog files, without compiling software or gateware"
    }

def task_build():
    return {
        "actions": ["python3 ltmu_soc.py basys3 --build"],
        "file_dep": ["ltmu.py", "ltmu_soc.py"],
        "targets": ["build", "BUILDTIME"],
        "doc": "Build the software and gateware"
    }

def task_builduartbone():
    return {
        "actions": ["python3 ltmu_soc.py --build --uart-name=crossover+uartbone --csr-csv csr.csv"],
        "file_dep": ["ltmu.py", "ltmu_soc.py"],
        "doc": "Builds the software and gate and additionally includes the UARTBone component"
    }

def task_cleanup():
    return {
        "actions": ["rm -rf build/", "rm BUILDTIME", "rm ltmu.vcd"],
        "doc": "Remove the previous build and debug files and folders"
    }

def task_test():
    return {
        "actions": [ltmu_test.main],
        "file_dep": ["ltmu.py", "ltmu_test.py"],
        "doc": "Run the LTMU tests"
    }