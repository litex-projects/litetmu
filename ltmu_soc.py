# This file is Copyright (c) 2014-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2013-2014 Sebastien Bourdeauducq <sb@m-labs.hk>
# License: BSD

# Modified 05.05.2022

import os
import argparse
import importlib

from migen import *

from litex.build.io import CRG

from litex.soc.integration.soc_core import *
from litex.soc.integration.builder import *
from litex.soc.cores.uart import UARTWishboneBridge
from litex.soc.cores.led import LedChaser
from litex.soc.integration.soc import SoCRegion
from liteeth.phy import LiteEthPHY
from litex.soc.doc import generate_docs
from crg import _CRG

from ltmu import *
from litepcie.software import *

from litex.soc.integration.builder import soc_directory, Builder

class BaseSoC(SoCCore):
    def __init__(self, platform, **kwargs):
        sys_clk_freq = int(1e9 / platform.default_clk_period)
	# kwargs["uart_name"]="serial"

        # SoCCore ----------------------------------------------------------------------------------
        SoCCore.__init__(self, platform, sys_clk_freq,
                         ident="LiteX SoC featuring the VexRiscV softcore and the LiteBusMonitor monitoring core",
                         #cpu_variant="standard+debug",
                         **kwargs)

        # CRG --------------------------------------------------------------------------------------
        self.submodules.crg = _CRG(platform, sys_clk_freq)
        self.crg.cd_sys.clk.attr.add("keep")
        self.platform.add_period_constraint(self.crg.cd_sys.clk, 1e9 / sys_clk_freq)


        # TMU ------------------
        self.tmu = WishboneMonitor(self.bus.masters["cpu_bus1"], ADDRESS_MODE.WORD_ADDRESSED)
        self.submodules += self.tmu







# Build --------------------------------------------------------------------------------------------

def main():
    parser = argparse.ArgumentParser(description="GDB over UART SoC")
    parser.add_argument("--build", action="store_true", help="Build bitstream")
    builder_args(parser)
    soc_core_args(parser)
    parser.add_argument("platform", help="Module name of the platform to build for")
    parser.add_argument("--toolchain", default=None, help="FPGA gateware toolchain used for build")


    args = parser.parse_args()

    platform_module = importlib.import_module(args.platform)
    if args.toolchain is not None:
        platform = platform_module.Platform(toolchain=args.toolchain)
    else:
        platform = platform_module.Platform()


    soc = BaseSoC(platform, **soc_core_argdict(args))
    builder = Builder(soc, **builder_argdict(args))
    

    generate_docs(soc, "build/documentation")
    builder.build(run=args.build)
    generate_litepcie_software_headers(soc, "../../litex/litepcie/litepcie/software/kernel/")
    print(os.path.abspath("../../litex/litepcie/litepcie/software/kernel/"))



if __name__ == "__main__":
    main()
