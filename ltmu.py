from migen import Module
import time
from termcolor import colored
from typing import List, Dict
from litex.gen.common import Reduce
import litex.soc.interconnect.wishbone as wb
from litex.soc.interconnect.stream import *
from litex.soc.interconnect.csr import *
import sys


def mprint(sender : str, s : str, c : str = "green"):
    print(colored(f"[{sender}] " ,c) + s)

def errprint(sender : str, s : str):
    mprint(sender, s, "red")

# _________________________________ CONSTANTS _________________________________
class COMPRESSION(IntEnum):
    SAME_ADDRESS = 0
    INCREASING_ADDRESS = 1
    DECREASING_ADDRESS = 2
    NONE = 3
    FOUR_BEAT_WRAP = 4
    EIGHT_BEAT_WRAP = 5
    SXTN_BEAT_WRAP = 6


class WB_CTI(IntEnum):
    CLASSIC = 0
    CONST_ADDR_BURST = -3
    INCREMENT_ADDR_BURST = -2
    END_OF_BURST = 6


# Burst Type Extension
class WB_BTE(IntEnum):
    LINEAR = 0
    FOUR_BEAT_WRAP = 1 # Lowest 4 bit are cyclic
    EIGHT_BEAT_WRAP = 2 # Lowest 8 bit are cyclic
    SXTN_BEAT_WRAP = 3 # Lowest 16 bit are cyclic

class ADDRESS_MODE(IntEnum):
    WORD_ADDRESSED = 0
    BYTE_ADDRESSED = 1


import math

# _________________________________ RECORDER _________________________________
# This module recognizes valid wishbone transactions and puts them into a queue, without further modification
class WishboneMonitorRecorder(Module, AutoCSR):
    def __init__(self, bus: wb.Interface, signal_limits : Dict[str,int], record_queue_capacity : int = 10):
        wrprint = lambda x: mprint("WishboneMonitorRecorder", x, "magenta")
        self.signal_limits = signal_limits
        compression_counter_bits = math.ceil(math.log(self.signal_limits["compression_counter"], 2))
        waitstate_counter_bits = math.ceil(math.log(self.signal_limits["waitstate_counter"], 2))
        idle_counter_bits = math.ceil(math.log(self.signal_limits["idle_counter"], 2))

        self.record_csr = CSRStorage(1, name="toggle_ltmu_recording", reset=0)

        self.record_layout = [
            ("adr", bus.adr_width),
            ("success", 1),
            ("we", 1),
            ("cti", 3),
            ("bte", 2),
            ("burst_duration", compression_counter_bits),
            ("idle_counter", idle_counter_bits),
            ("waitstate_counter", waitstate_counter_bits)
        ]

        wrprint("Buffer / Record - FIFO capacity: " + str(record_queue_capacity))
        wrprint("Recorder layout:")
        print("\n".join([k + " - " + str(v) + " bits" for k,v in self.record_layout]))

        self.record_fifo = record_fifo = SyncFIFO(self.record_layout, record_queue_capacity, buffered=True)
        self.submodules += record_fifo

        potential_address = Signal(bus.adr_width)
        potential_we = Signal()
        potential_success = Signal()
        potential_waitstate = Signal(waitstate_counter_bits)
        idle_cycles = Signal(idle_counter_bits)
        just_fired = Signal()
        last_burst_type = Signal(3)



        # RECORDING --------------------------------------------------------------------
        # Recording information from buffer into queue
        self.comb += [
            record_fifo.sink.adr.eq(potential_address),
            record_fifo.sink.we.eq(potential_we),
            record_fifo.sink.success.eq(potential_success),
            record_fifo.sink.idle_counter.eq(idle_cycles),

            If(potential_waitstate > 0,
                record_fifo.sink.waitstate_counter.eq(potential_waitstate - 1)  # Mitigate that every bus transaction takes _atleast_ 1 cycle
            ).Else(
                record_fifo.sink.waitstate_counter.eq(0)
            ),

            If(bus.cti != WB_CTI.CLASSIC,
                record_fifo.sink.cti.eq(last_burst_type)
            ).Else(
                record_fifo.sink.cti.eq(bus.cti),
            ),

            record_fifo.sink.bte.eq(bus.bte),
            If(potential_waitstate != (self.signal_limits["waitstate_counter"]),
                potential_success.eq(bus.stb & bus.ack & (~bus.err))
            ).Else(
                potential_success.eq(1) # When a new entry is created to account for the overflow, the first is always counted as successfull
            )
        ]

        

        # Committing new entries into the recording queue
        self.comb += [
            If(self.record_csr.storage,
                If(bus.cti == WB_CTI.CLASSIC,
                    If(bus.stb & bus.ack | (potential_waitstate == (self.signal_limits["waitstate_counter"])),
                        record_fifo.sink.valid.eq(1),
                        just_fired.eq(1)
                    ).Else(
                        record_fifo.sink.valid.eq(0),
                        just_fired.eq(0)
                    )
                ).Else(
                    If((bus.stb & bus.ack & (bus.cti == WB_CTI.END_OF_BURST)) | (potential_waitstate == (self.signal_limits["waitstate_counter"])),
                        record_fifo.sink.valid.eq(1),
                        record_fifo.sink.cti.eq(last_burst_type),
                        just_fired.eq(1)
                    ).Else(
                        just_fired.eq(0)
                    )
                )
           )
        ]
                        

        # Utility
        self.sync += [
            # Save last burst type
            If((bus.cti != WB_CTI.CLASSIC) & (bus.cti != WB_CTI.END_OF_BURST),
                last_burst_type.eq(bus.cti) # Needed so that not END_OF_BURST but the type of burst itself gets recorded
            ),

            # Save metadata to store into buffer
            If(bus.stb,
                potential_address.eq(bus.adr),
                potential_we.eq(bus.we),
            ),

            # Recording waitstates, idle cycles and burst durations
            If(bus.cti == WB_CTI.CLASSIC,
                # CLASSIC MODE
                If(bus.stb & ~bus.ack & ~just_fired,
                    potential_waitstate.eq(potential_waitstate + 1),
                ) 
            ).Else(
                # BURST MODE
                If(bus.ack & (bus.cti == WB_CTI.CONST_ADDR_BURST) & (~just_fired),
                    record_fifo.sink.burst_duration.eq(record_fifo.sink.burst_duration + 1)
                )
            ),

            # Reset the data
            If(just_fired,
                record_fifo.sink.burst_duration.eq(0),
                idle_cycles.eq(0),
                potential_waitstate.eq(0),
            ),

            # Increase the idle cycle counter in case no transaction is happening
            If((~bus.cyc | ~bus.stb) & (idle_cycles < self.signal_limits["idle_counter"]) & (~just_fired),
                idle_cycles.eq(idle_cycles + 1)
            )
        ] 

        # Instead of having an own endpoint just use the fifos endpoint
        self.source = record_fifo.source






# _________________________________ COMPRESSOR _________________________________
# Compresses data coming from a WishboneMonitorRecorder (or an endpoint using a similar queue layout)
class WishboneMonitorCompressor(Module, AutoCSR):
    def __init__(self, record_fifo : WishboneMonitorRecorder, bus : wb.Interface, capacity : int, address_increase_continuous : int,
                signal_limits : Dict[str,int], compression_counter_bits : int = 8, waitstate_counter_bits : int = 8, idle_counter_bits : int = 8):
        wcprint = lambda x: mprint("WishboneMonitorCompressor", x, "blue")
        self.signal_limits = signal_limits

        # DEFINITIONS | PRINTS --------------------------------------------------------------------
        self.compression_layout = [
            ("adr", bus.adr_width),
            ("success", 1),
            ("we", 1),
            ("compression_type", 3),
            ("compression_counter", compression_counter_bits),
            ("waitstate_counter", waitstate_counter_bits),
            ("idle_counter", idle_counter_bits),
            ("parity", 1)
        ]
        entry_bit_size = sum([x for _,x in self.compression_layout]) 
        mem_capcity = (entry_bit_size * capacity) // 8 + 1              # In bytes
        mem_capcity_kb = mem_capcity // 1000
        
        wcprint("Bits required per entry: " + colored(str(entry_bit_size), "yellow"))
        wcprint("Bytes available for temporary storage: " + colored(str(mem_capcity), "yellow") + " (" + colored(str(mem_capcity_kb) + " KB", "yellow") + ")") 
        wcprint("Fastest memory fill after " + colored(str((capacity / (100000000)) * 1000) + " ms", "yellow") + " (using a 100 MHz clock, worst case compression (none), maximum traffic (1 transaction every cycle) and no readout while recording)")
        wcprint("Layout of the compressed data: ")
        print("\n".join([k + " - " + str(v) + " bits" for k,v in self.compression_layout]))
        wcprint("Maximum values for the counter variables:")
        print("\n".join([k + " - " + hex(v) for k,v in self.signal_limits.items()]))


        # COMPRESSION --------------------------------------------------------------------
        loaded_data = dict([(name, Signal(leng)) for name, leng in record_fifo.record_layout])
        compression_type = Signal(3)
        compression_counter = Signal(8)

        self.compressed_fifo = SyncFIFO(self.compression_layout, depth=capacity, buffered=True)
        self.submodules += self.compressed_fifo

        # Use fifo endpoint instead of defining own endpoint
        self.source = self.compressed_fifo.source

        # Always read when data available
        self.comb += record_fifo.source.ready.eq(1)
        
        # Parity valid data
        pdata = Cat([self.compressed_fifo.sink.adr, self.compressed_fifo.sink.we, self.compressed_fifo.sink.success, self.compressed_fifo.sink.compression_type, self.compressed_fifo.sink.compression_counter, self.compressed_fifo.sink.waitstate_counter, self.compressed_fifo.sink.idle_counter])
        wcprint("LEN CAT " + str(len(pdata)))
        
        # Parity is always the same
        self.comb += self.compressed_fifo.sink.parity.eq(Reduce("XOR", [pdata[i] for i in range(len(pdata))]))

        self.sync += [
            # Disable commiting of new data when the incoming data is invalid (every transaction needs a successfull one to follow it)
            If(~record_fifo.source.valid,
                self.compressed_fifo.sink.valid.eq(0)
            ),

            # Compress
            If(record_fifo.source.valid,
                If(record_fifo.source.cti != WB_CTI.CLASSIC,
                    # BURST ISSUED BY THE BUS
                    self.load_new_data_from_record_queue(loaded_data, record_fifo),
                    self.commit_to_output_fifo(loaded_data, compression_type, compression_counter, is_burst=True)
                ).Else(
                    # TMU COMPRESSION
                    # (self.compress_data() also commits the data)
                    self.load_new_data_from_record_queue(loaded_data, record_fifo),
                    self.compress_data(loaded_data, record_fifo, compression_type, compression_counter, address_increase_continuous)
                )
            ).Else(
                    self.compressed_fifo.sink.valid.eq(0)
            )
        ]

    
    def commit_to_output_fifo(self, loaded_data : Dict[str, Signal], compression_type : Signal, compression_counter : Signal, is_burst : bool) -> List:
        """These assignments are issued when an entry is compressed and ready to be stored in the temporary fifo memory"""
        commands = [
            self.compressed_fifo.sink.adr.eq(loaded_data["adr"]),
            self.compressed_fifo.sink.we.eq(loaded_data["we"]),
            self.compressed_fifo.sink.success.eq(loaded_data["success"]),
            self.compressed_fifo.sink.waitstate_counter.eq(loaded_data["waitstate_counter"]),
            self.compressed_fifo.sink.idle_counter.eq(loaded_data["idle_counter"]),
            self.compressed_fifo.sink.valid.eq(1)
        ]

        if not is_burst:
            commands += [
                self.compressed_fifo.sink.compression_type.eq(compression_type),
                self.compressed_fifo.sink.compression_counter.eq(compression_counter),
                compression_type.eq(COMPRESSION.NONE),
                compression_counter.eq(0),
            ]
        else:
            commands += [
                self.compressed_fifo.sink.compression_type.eq(loaded_data["cti"] + 3), # 3 is the offset in the enum of compression_type
                self.compressed_fifo.sink.compression_counter.eq(loaded_data["burst_duration"]),
            ]
        return commands


    def load_new_data_from_record_queue(self, loaded_data, record_fifo):
        return [loaded_data[name].eq(record_fifo.source.__getattr__(name)) for name, _ in record_fifo.record_layout]


    def compress_data(self, loaded_data, record_fifo, compression_type, compression_counter, address_increase_continuous):
        return [
            # Compression of classic entries
            If((record_fifo.source.adr == loaded_data["adr"] + address_increase_continuous) & (record_fifo.source.we == loaded_data["we"]) & (record_fifo.source.success == loaded_data["success"]) & ((compression_type == COMPRESSION.NONE) | (compression_type == COMPRESSION.INCREASING_ADDRESS)) & (compression_counter < self.signal_limits["compression_counter"]),
                compression_type.eq(COMPRESSION.INCREASING_ADDRESS),
                compression_counter.eq(compression_counter + 1),
                loaded_data["adr"].eq(loaded_data["adr"] + address_increase_continuous),
                self.compressed_fifo.sink.valid.eq(0),
            ).Elif((record_fifo.source.adr == loaded_data["adr"] - address_increase_continuous) & (record_fifo.source.we == loaded_data["we"]) & (record_fifo.source.success == loaded_data["success"]) & ((compression_type == COMPRESSION.NONE) | (compression_type == COMPRESSION.DECREASING_ADDRESS)) & (compression_counter < self.signal_limits["compression_counter"]),
                compression_type.eq(COMPRESSION.DECREASING_ADDRESS),
                compression_counter.eq(compression_counter + 1),
                loaded_data["adr"].eq(loaded_data["adr"] - address_increase_continuous),
                self.compressed_fifo.sink.valid.eq(0),
            ).Elif((record_fifo.source.adr == loaded_data["adr"]) & (record_fifo.source.we == loaded_data["we"]) & (record_fifo.source.success == loaded_data["success"]) & ((compression_type == COMPRESSION.NONE) | (compression_type == COMPRESSION.SAME_ADDRESS)) & (compression_counter < self.signal_limits["compression_counter"]) & (loaded_data["waitstate_counter"] != self.signal_limits["waitstate_counter"] - 1),  # When a new entry is issued because of a waitstate counter overflow, this is not a compressed transaction, and so it shouldn't be triggered as one
                compression_type.eq(COMPRESSION.SAME_ADDRESS),
                compression_counter.eq(compression_counter + 1),
                loaded_data["adr"].eq(loaded_data["adr"]),
                self.compressed_fifo.sink.valid.eq(0),
            ).Else(
                *(self.commit_to_output_fifo(loaded_data, compression_type, compression_counter, is_burst=False))
            )
        ]




# _________________________________ MONITOR _________________________________
class WishboneMonitor(Module, AutoCSR):
    def __init__(self, bus : wb.Interface, adr_mode : ADDRESS_MODE, capacity : int = 8192, compression_counter_bits : int = 8, waitstate_counter_bits : int = 8, idle_counter_bits : int = 8) -> None:
        if type(bus) != wb.Interface:
            print("ERROR: bus argument passed to the WishboneMonitor must be of type wishbone.Interface!")
            sys.exit()
        
        wbprint = lambda x: mprint("WishboneMonitor", x)
        print()
        wbprint("START TMU ----------------------------------------------")
        wbprint("Creating a monitor for interface: " + bus.name)
        wbprint("Capacity: " + str(capacity))
        wbprint("Address mode: " + str(adr_mode.name))

        # Note which distance successive transactions ought to have
        adr_increase = -1
        if adr_mode == ADDRESS_MODE.BYTE_ADDRESSED:
            adr_increase = bus.data_width // 8
        elif adr_mode == ADDRESS_MODE.WORD_ADDRESSED:
            adr_increase = bus.data_width // 32

        self.signal_limits = {
            "compression_counter": 2 ** compression_counter_bits - 1,
            "waitstate_counter": 2 ** waitstate_counter_bits - 1,
            "idle_counter": 2 ** idle_counter_bits - 1
        }
        
        # Recorder
        self.recorder = WishboneMonitorRecorder(bus, self.signal_limits, 10) # This capacity only determines the buffer size, not memory size
        self.submodules += self.recorder

        # Compressor
        self.compressor = WishboneMonitorCompressor(self.recorder, bus, capacity, adr_increase, self.signal_limits, compression_counter_bits, waitstate_counter_bits, idle_counter_bits)
        self.submodules += self.compressor
        self.compression_layout = self.compressor.compression_layout
        self.compressed_fifo = self.compressor.compressed_fifo
        

        # READOUT
        self.create_csrs()

        # Insert data from queue into CSRs according to their matching names
        for name, csr in self.csrs.items():
            self.sync += csr.status.eq(self.compressor.source.__getattr__(name))
        
        wbprint("Created CSR Registers: \n" + "\n".join([c.name for c in self.csrs.values()]) + "\n")
        

        # Status register whether an entry can be read from the queue
        self.entry_available_csr = CSRStatus(32, name="entries_available")
        self.sync += [
            self.entry_available_csr.status.eq(self.compressor.compressed_fifo.level)
        ]

        self.debugcsr = CSRStatus(32, name="debug")
        self.sync += self.debugcsr.status.eq(self.debugcsr.status + bus.stb)

        # Status register to check whether an overflow has happened yet
        self.overflow_csr = CSRStatus(1, reset=0, name="overflow_flag")
        self.sync += [
            If(~self.recorder.record_csr.storage,
                self.overflow_csr.status.eq(0)
            ).Else(
                If(self.overflow_csr.status | ((self.compressed_fifo.level+1) == capacity),
                    self.overflow_csr.status.eq(1)
                )
            )
        ]

        # Version Checking CSRs
        self.build_seconds = build_seconds = int(time.time())
        self.version_csr = CSRStatus(32, name="version", reset=0x756d74)                # 0x746d75 = 116 109 117 = tmu (ascii) (backwards: little endian)
        self.build_time_csr = CSRStatus(32, name="build_time", reset=build_seconds)     # Time at which the TMU was built
        wbprint("Build time (checkable in build_time CSR): " + str(build_seconds))
        with open("BUILDTIME", "w+") as f:
            f.write(str(build_seconds))


        # FIFO level CSR
        self.compressed_fifo_level_csr = CSRStatus(size=32, name="fifo_level")
        self.sync += self.compressed_fifo_level_csr.status.eq(self.compressed_fifo.level)

        wbprint("END TMU ----------------------------------------------\n")

    # Create all CSRs needed to read out data
    def create_csrs(self):
        self.csr_adr = CSRStatus(self.compression_layout[0][1], name=self.compression_layout[0][0])    
        self.csr_we = CSRStatus(self.compression_layout[2][1], name=self.compression_layout[2][0])    
        self.csr_success = CSRStatus(self.compression_layout[1][1], name=self.compression_layout[1][0])    
        self.csr_compression_type = CSRStatus(self.compression_layout[3][1], name=self.compression_layout[3][0])    
        self.csr_compression_counter = CSRStatus(self.compression_layout[4][1], name=self.compression_layout[4][0])    
        self.csr_waitstate_counter = CSRStatus(self.compression_layout[5][1], name=self.compression_layout[5][0])    
        self.csr_idle_counter = CSRStatus(self.compression_layout[6][1], name=self.compression_layout[6][0])    
        self.csr_parity = CSRStatus(self.compression_layout[7][1], name=self.compression_layout[7][0])

        # Register to write to in order to request the next entry in the queue
        self.next_entry_csr = next_entry_csr = CSRStorage(1, write_from_dev=True, name="request_next")

        self.csrs = {
            "adr": self.csr_adr,
            "we": self.csr_we,
            "success": self.csr_success,
            "compression_type": self.csr_compression_type,
            "compression_counter": self.csr_compression_counter,
            "waitstate_counter": self.csr_waitstate_counter,
            "idle_counter": self.csr_idle_counter,
            "parity": self.csr_parity
        }

        # Check if all data has a CSR register available
        if set(self.csrs.keys()) != set([n for n,l in self.compression_layout]):
            errprint("WishboneMonitor", "ERROR: List of CSRs \n" + str(list(self.csrs.keys())) + "\n does not match layout of compressed data \n" + str([n for n,_ in self.compression_layout]))
            sys.exit()

        self.sync += [
            If(next_entry_csr.storage,
                next_entry_csr.storage.eq(0),
                self.compressed_fifo.source.ready.eq(1)
            ).Else(
                self.compressed_fifo.source.ready.eq(0)
            ),
        ]
