# LiteTMU (LTMU)
The LiteTMU is a **T**rust **M**onitoring **U**nit for application in the LiteX framework. It can record transactions by monitoring a bus master interface and presents these transactions via CSR Registers.

The ```ltmu.py``` file contains 3 main classes:
* WishboneMonitor: The TMU itself (responsible for providing the data from the buffer FIFOs in the CSRs)
* WishboneMonitorRecorder: Receives an interface and puts the recorded transactions into a buffer FIFO
* WishboneMonitorCompressor: Compresses the incoming entries from the recorder and stores them in a much larger FIFO 

The content-CSRs (containing data from an entry) are found in self.csrs. The meta CSRs are (referencing their name argument, which also appears in csr.h)
* version: A constant CSR containg the string "tmu" in ASCII form (to confirm the existance of the TMU on the board)
* build_time: A constant CSR containg the epoch seconds at which the board gateware and software were built. Must be equivalent to the content of the BUILDTIME file, which is being written when a build is done (independent of synthesis etc.)
* request_next: Set to 1 and then to 0 immediately after to load the next entries' data into the content CSRs
* entries_available: The entries current in the large buffer FIFO
* overflow_flag: Is set to 1 and stays at 1 if at any point the FIFOs capacity was fully used (and thus a recorded entry had to be discarded)

## Usage
Include the TMU in your SoC:
```Python
self.tmu = WishboneMonitor(busint, ADDRESS_MODE.WORD_ADDRESSED)
self.submodules += self.tmu
```

In this example busint is of the type litex.soc.interconnect.wishbone.Interface (it must be). Commonly the CPU's busses are monitored, e.g.: ```self.bus.masters["cpu_bus0"]```
The second argument defines the spacing in which entries are compressed. (With 1 byte or 4 byte difference?)


## Reading data directly using UARTBone
* ```py litex_tmu_soc.py basys3 --build --uart-name=crossover+uartbone --csr-csv csr.csv```
* ```litex_server.py --uart --uart-port=/dev/ttyUSB1```
* ```litex_client.py --regs --csr-csv csr.csv```
* ```litex_term.py crossover```
