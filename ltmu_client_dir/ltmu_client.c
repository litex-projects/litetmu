#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <unistd.h>
#include <math.h>
#include <litepcie.h>
#include <csr.h>
#include <fcntl.h>
#include "liblitepcie.h"
#include <signal.h>
#include <sys/stat.h>
#include "ltmu_client.h"
#include <time.h>


int pcie;     // The filedescriptor for the litepcie device returned from open()
FILE* data_f; // The filedescriptor for the log file to which raw bytes get written

int uints_written = 0;

// Load next entry
void load_next(struct litepcie_device *s) {
    litepcie_writel(s, CSR_TMU_REQUEST_NEXT, 1);
    litepcie_writel(s, CSR_TMU_REQUEST_NEXT, 0);
}

// Enable recording
void enable_recording(struct litepcie_device *s) {
    litepcie_writel(s, CSR_TMU_RECORDER_TOGGLE_LTMU_RECORDING, 1);
}

// Disable recording
void disable_recording(struct litepcie_device *s) {
    litepcie_writel(s, CSR_TMU_RECORDER_TOGGLE_LTMU_RECORDING, 0);
}

// Return whether the compression queue has overflowed since last enabling of recording
uint32_t check_overflow(struct litepcie_device *s) {
    return (litepcie_readl(s, CSR_TMU_OVERFLOW_FLAG) & 0x1);
}

// Read the current entry from PCIe and write it into the open file buffer
void _write_to_log_file(struct litepcie_device *s) {
    while (litepcie_readl(s, CSR_TMU_ENTRIES_AVAILABLE) <= 0);

    uint32_t buf[7];
    buf[0] = litepcie_readl(s, CSR_TMU_ADR);
    buf[1] = litepcie_readl(s, CSR_TMU_SUCCESS) ;   // TODO: Masking needed?
    buf[2] = litepcie_readl(s, CSR_TMU_WE) ;
    buf[3] = litepcie_readl(s, CSR_TMU_COMPRESSION_TYPE);
    buf[4] = litepcie_readl(s, CSR_TMU_COMPRESSION_COUNTER);
    buf[5] = litepcie_readl(s, CSR_TMU_WAITSTATE_COUNTER);
    buf[6] = litepcie_readl(s, CSR_TMU_IDLE_COUNTER);
    uints_written += fwrite(&buf, sizeof(uint32_t), 7, data_f);
}

// Wait for atleast one entry is available
volatile void wait_for_entry(struct litepcie_device *s) {
    // Wait until the first entry is available
    while (1) {
        if (litepcie_readl(s, CSR_TMU_ENTRIES_AVAILABLE) > 0) {
            break;
        }
    }
}


int main(int argc, char* argv[]) {
    // Open device
    pcie = open(DEVICE, O_RDWR);
    if (pcie < 0) {
        printf("Error connecting to PCIe device!\n");
        exit(1);
    }


    // Check build-time and signature
    uint32_t sign = litepcie_readl(pcie, CSR_TMU_VERSION);
    if (sign != SIGN_CONST) {
        printf("ERROR: Expected sign constant to be 0x%lx, but received 0x%lx  instead!\n", SIGN_CONST, sign);
        exit(1);
    }
    printf("SIGN: 0x%lx\n", sign);
    printf("BUILDTIME: %lu\n\n", litepcie_readl(pcie, CSR_TMU_BUILD_TIME));

    // Write to file (number of entries given)
    data_f = fopen("ltmu_data", "w+");
    CHECK_NULL(data_f, "[Recorder] Error creating the log file to write to!\n");
    
    printf("Waiting for TMU to fill buffer...\n");
    int elems = 0;

    // Record time for benchmarking
    time_t start_time = time(NULL);

    // Enable the TMU by writing 1 into its enable register
    enable_recording(pcie);

    // The queue of the TMU may not be immediately filled
    wait_for_entry(pcie);
    
    for (int i = 0; i < ENTRIES; i++) {
        _write_to_log_file(pcie);
        load_next(pcie);
        if (i % 100 == 0) {
            printf("%d / %d (Overflowed yet: %lu)\n", i, ENTRIES, check_overflow(pcie));
        } 
    }

    // Disable TMU before ending the program
    disable_recording(pcie);

    // Benchmarking
    double seconds_used = difftime(time(NULL), start_time);

    // Close connection to PCIe device
    close(pcie);


    // Close data log file
    fclose(data_f);
    printf("%d bytes of data read from PCIe (in %f seconds, %f entries per second) and written to log file!\n", uints_written * sizeof(uint32_t), seconds_used, ENTRIES/(seconds_used));

    return 0;
}