#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include "ltmu_client.h"

FILE* fd;
int written = 0;
int read_bytes = 0;

void _wtofile() {
    uint32_t buf[7];
    buf[0] = 0xA;
    buf[1] = 0xB;
    buf[2] = 0xC;
    buf[3] = 0xD;
    buf[4] = 0xE;
    buf[5] = 0xF;
    buf[6] = 0x8;

    written += (fwrite(&buf, sizeof(uint32_t), 7, fd)) * sizeof(uint32_t);
}


void print_entry(data_entry* dat) {
    printf("a: 0x%lx | s: %lu | w: %lu | ct: %lu | cc: %lu | wc: %lu | ic: %lu\n",
        dat->address,
        dat->success,
        dat->write_enable,
        dat->compression_type,
        dat->compression_counter,
        dat->waitstate_counter,
        dat->idle_counter
    );
}


int main() {


    // WRITING
        fd = fopen("testdatafile", "w+");
    for (int i = 0; i < ENTRIES; i++) {
        _wtofile();
    }
    printf("Wrote %d bytes\n", written);
    fclose(fd);



    // READING
    fd = fopen("testdatafile", "r");
    

    data_entry** datas = malloc(sizeof(data_entry*) * ENTRIES); 

    for (int i = 0; i < ENTRIES; i++) {
        datas[i] = malloc(sizeof(data_entry));
        read_bytes += (fread(datas[i], sizeof(data_entry), 1, fd)) * sizeof(data_entry); 
    }

    printf("Read %d bytes\n", read_bytes);
    

    for (int i = 0; i < 5; i++) {
        print_entry(datas[i]);
    }

    fclose(fd);

    for (int i = 0; i < ENTRIES; i++) {
        free(datas[i]);
    }
    free(datas);

    return 0;

}