#include "ltmu_client.h"
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>


FILE* f;
data_entry** datas; 
int entries_read = 0;

void print_entry(data_entry* dat) {
    printf("a: 0x%lx | s: %lu | w: %lu | ct: %lu | cc: %lu | wc: %lu | ic: %lu\n",
        dat->address,
        dat->success,
        dat->write_enable,
        dat->compression_type,
        dat->compression_counter,
        dat->waitstate_counter,
        dat->idle_counter
    );
}

// Reserve space for <elements> entries
data_entry** malloc_data(int elements) {
    data_entry **t = malloc(sizeof(data_entry*) * elements);
    for (int i = 0; i < elements; i++) {
        t[i] = malloc(sizeof(data_entry));
    }
    return t;
}


int data_entry_to_json(data_entry* d, char** buffer) {
    int written = sprintf(*buffer, "\t{\n\t\t\"address\": %lu,\n\t\t\"success\": %lu,\n\t\t\"we\": %lu,\n\t\t\"compression_type\": %lu,\n\t\t\"compression_counter\": %lu,\n\t\t\"waitstate_counter\": %lu,\n\t\t\"idle_counter\": %lu\n\t},");
    if (written < 0) {
        printf("Error while converting entry to JSON!\n");
        exit(1);
    }
    return written;
}


void write_json(data_entry** datas, int elements) {
    FILE* json = fopen("data.json", "w+");
    char buffer[400] = "[\n";
    int buffer_used; // in chars
    int chars_written = fwrite(buffer, sizeof(char), 2, json);
    for (int i = 0; i < elements; i++) {
        buffer_used = data_entry_to_json(datas[i], &buffer);
        chars_written += fwrite(buffer, sizeof(char), buffer_used, json);
    }
    chars_written += fwrite("]", sizeof(char), 1, json);
    fclose(json);
    printf("Wrote the data points into a json file (Wrote %d entries, %d chars)\n", elements, chars_written);
}


// Free space of <elements> entries
void free_data(data_entry** d, int elements) {
    for (int i = 0; i < elements; i++) {
        free(d[i]);
    }
    free(d);
}


int main(int argc, char* argv[]) {
    if (argc < 2)  {
        printf("Filename required! (ltmu_analyzer <binary-file>)\n");
        exit(1);
    }

    // Read file
    f = fopen(argv[1], "r");
    CHECK_NULL(f, "[Analyzer] Error opening the log file\n");

    // Read entries directly into structs
    datas = malloc_data(ENTRIES);
    for (int i = 0; i < ENTRIES; i++) {
        entries_read += fread(datas[i], sizeof(data_entry), 1, f);
        print_entry(datas[i]);
    }

    printf("Read %d bytes!\n", entries_read * sizeof(data_entry));
    printf("Writing to data.json...\n");
    write_json(datas, ENTRIES);

    free_data(datas, ENTRIES);
    return 0;
}