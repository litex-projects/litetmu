from litex.tools.litex_client import *
from tmu.ltmu import *
import sys
from typing import Dict, Any


class LTMUClient(RemoteClient):
    def __init__(self, tmu_name_prefix="tmu_", host="localhost", port=1234, base_address=0, csr_csv=None, csr_data_width=None, debug=False):
        super().__init__(host, port, base_address, csr_csv, csr_data_width, debug)
        self.tmu_name_prefix = tmu_name_prefix

    def addr_of_reg(self, name : str) -> int:
        if name not in self.regs.__dict__.keys():
            print("ERR: Register with name " + name + " not found!")
            sys.exit()
        return self.regs.__dict__[name].addr


    def get_next_entry(self):
        self.write(self.regs.__dict__[self.tmu_name_prefix + "request_next"].addr, 1)
    

    def read_register(self, name : str) -> int:
        return self.read(self.addr_of_reg(name), 32)
    
    def write_register(self, name : str, data : Any):
        self.write(self.addr_of_reg(name), data) 


    def read_data(self) -> Dict:
        d = {}
        lst = [self.tmu_name_prefix + name for name in ["adr", "we", "success", "compression_type", "compression_counter", "waitstate_counter", "idle_counter"]]
        for name, register in self.regs.__dict__.items():
            if name in lst:
                if name == self.tmu_name_prefix + "compression_type":
                    d[name] = COMPRESSION(register.read()).name
                elif name in [self.tmu_name_prefix + "we", self.tmu_name_prefix + "success"]:
                    d[name] = str(bool(register.read()))
                elif name in [self.tmu_name_prefix + "compression_counter", self.tmu_name_prefix + "waitstate_counter", self.tmu_name_prefix + "idle_counter"]:
                    d[name] = str(register.read())
                else:
                    d[name] = f"0x{register.read():08x}"
        return d
    
    def read_print_data(self, num : int):
        d = self.read_data()
        print("{:<20} {:<20} {:<20} {:<20} {:<20} {:<20} {:<20}".format(*d.keys()))
        for i in range(num):
            if self.regs.__dict__[self.tmu_name_prefix + "entries_available"].read() == 0:
                print("Ending readout, no more entries available!")
                return
            print("{:<20} {:<20} {:<20} {:<20} {:<20} {:<20} {:<20}".format(*list(self.read_data().values())))
            self.get_next_entry()
            


if __name__ == "__main__":
    lc = LTMUClient(csr_csv="csr.csv")
    print("Opening connection")
    lc.open()

    # Start recording
    print("Toggling recording on")
    assert "tmu_recorder_toggle_ltmu_recording" in lc.regs.__dict__.keys()
    lc.write_register("tmu_recorder_toggle_ltmu_recording", 1)
    assert lc.regs.__dict__["tmu_recorder_toggle_ltmu_recording"].read() == 1

    # Wait for interaction
    num = int(input("Number of entries to read (starts reading after pressing enter): "))
    lc.write_register("tmu_recorder_toggle_ltmu_recording", 0)
    assert lc.regs.__dict__["tmu_recorder_toggle_ltmu_recording"].read() == 0

    lc.read_print_data(num)

    lc.close()