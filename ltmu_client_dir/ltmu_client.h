#ifndef LTMU_CLIENT_H
#define LTMU_CLIENT_H

#define DEVICE "/dev/litepcie1"
#define ENTRIES 10000                       // Number of entries to read
#define BYTES_PER_ENTRY 7 * 4               // 7 Fields with 32 bit each

#define CHECK_NULL(f,msg) if (f == NULL) { printf(msg); exit(1); }

#define SIGN_CONST 0x756d74

struct __attribute__ ((__packed__)) data_entry {
    unsigned int address;
    unsigned int success;
    unsigned int write_enable;
    unsigned int compression_type;
    unsigned int compression_counter;
    unsigned int waitstate_counter;
    unsigned int idle_counter;
};
typedef struct data_entry data_entry;

void print_entry(data_entry*);

#endif