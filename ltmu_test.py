from migen import *
from tqdm import tqdm
import sys
from termcolor import colored
from ltmu import WishboneMonitor, WB_CTI, WB_BTE, COMPRESSION, ADDRESS_MODE
from litex.soc.interconnect import wishbone as wb
from litex.soc.integration.soc import SoCBusHandler, SoCRegion
import random
from bitstring import BitArray
from enum import IntEnum

MEM_SIZE = 0x1000
FIFO_CAPACITY = 1000
SIGN_CONST = 0x756d74 # TMU Backwards (little-endian)

TEST_SUCCESS = False    # Changed by the tb() function if successfull

def tprint(t : str):
    print(colored("Generating transactions: ", "blue") + colored(t, "blue"))

class WE(IntEnum):
    READ = 0
    WRITE = 1

class RESULT(IntEnum):
    SUCCESS = 1
    FAILURE = 0


class WaitedWBSRAM(Module):
    """Acts as a wishbone slave that answers after a given waitstate delay. Data is ignored / non-relevant for both reads and writes"""
    def __init__(self, interface : wb.Interface) -> None:
        accepting_requests = Signal(reset=1)
        self.reading_adr = address = Signal(32)
        self.delay = delay = Signal(32)

        self.delay_override = Signal(32)

        self.comb += interface.dat_r.eq(0xCE)


        self.comb += [
            # As long as STB and CYC are 1, the address presented is valid
            If(interface.stb & interface.cyc,
                address.eq(interface.adr),
            )
        ]

        self.sync += [
            If(interface.stb & interface.cyc & accepting_requests,
                If(self.delay_override == 0,
                    delay.eq(interface.adr & 0xF),      # Use the last 4 bit of the address as a delay timer
                ).Else(
                    delay.eq(self.delay_override)
                ),
                accepting_requests.eq(0)            # Do not accept new transmission until waitstates are done
            ),

            # Decrease delay every cycle, as long as the transaction is valid
            If(interface.stb & interface.cyc,
                If(~accepting_requests & (delay > 0),
                    delay.eq(delay - 1),
                ).Elif(~accepting_requests & (delay == 0),
                    accepting_requests.eq(1),
                )
            )
        ]

        self.comb += [
            If((delay == 0) & (~accepting_requests),
                interface.ack.eq(1)
            ).Else(
                interface.ack.eq(0)
            )
        ]


class DUT(Module):
    def __init__(self, address_mode : ADDRESS_MODE) -> None:
        # INTERFACES
        self.ram_bus = wb.Interface(data_width=32, adr_width=32, bursting=True) 
        self.master_bus = wb.Interface(data_width=32, adr_width=32, bursting=True)
        self.mock_sram_int = wb.Interface(data_width=32, adr_width=32, bursting=True)

        # COMPONENTS
        self.sram = sram = wb.SRAM(MEM_SIZE, self.ram_bus)
        self.submodules += sram

        self.mock_sram = WaitedWBSRAM(self.mock_sram_int)
        self.submodules += self.mock_sram

        # BUS LOGIC
        self.sbh = sbh = SoCBusHandler(standard="wishbone", data_width=32, address_width=32, interconnect="shared")
        self.submodules += sbh

        # IMPORTANT: SoCRegions are BYTE addressed, The wishbone interface is WORD addressed
        sbh.add_slave("ram slave", slave=sram.bus, region=SoCRegion(origin=0x0, size=MEM_SIZE*4, mode="rwx"))
        sbh.add_slave("mock sram", slave=self.mock_sram_int, region=SoCRegion(origin=MEM_SIZE*4, size=MEM_SIZE*4, mode="rwx"))
        sbh.add_master("test master", master=self.master_bus)
        
        print("BUS INTERCONNECT: " + sbh.interconnect)
        print("REGIONS: ", end="\n")
        for k,v in sbh.regions.items():
            print("\t" + k + ": " + str(hex(v.origin)) + " - " + str(hex(v.origin + v.size)))


        # MONITOR
        self.dut = WishboneMonitor(self.master_bus, address_mode, capacity=FIFO_CAPACITY)
        self.submodules += self.dut

        # Evil! Only for testing:
        self.dut.compressed_fifo = self.dut.compressor.compressed_fifo

        self.address_gap = self.master_bus.data_width//8


# VARIBALES
simulated_entries = []
simulated_entries_testtype = []
rng = random.Random()
dutobj = None
simulated_entry_count = 0
sys.tracebacklimit = 0

# Generate a random number, different form the previous one (to avoid compression on purpose)
# From 0 until MEM_SIZE (all memories in this test should be MEM_SIZE large)
def rand_diff_num(previous : int) -> int:
    num = -1
    while num < 0 or num == previous or (num + dutobj.address_gap) == previous or (num - dutobj.address_gap) == previous:
        num = rng.randint(1, MEM_SIZE-1)
    return num

# Queue in a check for an entry
def queue_for_check(adr, we, ct, cc, wc, ic, s, testtype):
    simulated_entries.append({
        "adr": adr,
        "we": we,
        "ct": ct,
        "cc": cc,
        "wc": wc,
        "ic": ic,
        "s": s
    })
    simulated_entries_testtype.append(testtype)

def _count_ones(num : int):
    return sum(map(int, bin(num)[2:]))

# Calculate the parity for the current compressed_fifo outputs (exluding the parity field itself)
def _calculate_parity():
    x = 0
    x += _count_ones((yield dutobj.dut.compressed_fifo.source.adr))
    x += _count_ones((yield dutobj.dut.compressed_fifo.source.we))
    x += _count_ones((yield dutobj.dut.compressed_fifo.source.success))
    x += _count_ones((yield dutobj.dut.compressed_fifo.source.compression_type))
    x += _count_ones((yield dutobj.dut.compressed_fifo.source.compression_counter))
    x += _count_ones((yield dutobj.dut.compressed_fifo.source.waitstate_counter))
    x += _count_ones((yield dutobj.dut.compressed_fifo.source.idle_counter))
    return x % 2   # Return 1 if num of 1s is odd

# Read from the queue and make sure the data is correct
def check(adr, we, ct, cc, wc, ic, s):
    yield dutobj.dut.compressed_fifo.source.ready.eq(1)
    yield
    
    assert (yield dutobj.dut.compressed_fifo.source.adr) == adr, hex((yield dutobj.dut.compressed_fifo.source.adr)) + " adr " + hex(adr)
    assert (yield dutobj.dut.compressed_fifo.source.we) == we, str((yield dutobj.dut.compressed_fifo.source.we)) + " we " + str(we)
    assert (yield dutobj.dut.compressed_fifo.source.compression_type) == ct, str(COMPRESSION((yield dutobj.dut.compressed_fifo.source.compression_type)).name) + " ct " + str(ct)
    assert (yield dutobj.dut.compressed_fifo.source.compression_counter) == cc, str((yield dutobj.dut.compressed_fifo.source.compression_counter)) + " cc " + str(cc)
    assert (yield dutobj.dut.compressed_fifo.source.waitstate_counter) == wc, str((yield dutobj.dut.compressed_fifo.source.waitstate_counter)) + " wc " + str(wc)
    assert (yield dutobj.dut.compressed_fifo.source.idle_counter) == ic, str((yield dutobj.dut.compressed_fifo.source.idle_counter)) + " ic " + str(ic)
    assert (yield dutobj.dut.compressed_fifo.source.success) == s, str((yield dutobj.dut.compressed_fifo.source.success)) + " s " + str(s)
    assert (yield dutobj.dut.compressed_fifo.source.parity) == (yield from _calculate_parity()), f"{(yield dutobj.dut.compressed_fifo.source.parity)} parity {(yield from _calculate_parity())}"
    yield dutobj.dut.compressed_fifo.source.ready.eq(0)


# Run checks on all the data
def check_all_queued_entries():
    checked_index = 0
    try:
        # Check sign and buildtime
        with open("BUILDTIME", 'r') as f:
            build_time_saved = int(f.read())
            assert build_time_saved == (yield dutobj.dut.build_time_csr.status), f"Build time check error: Saved build time {build_time_saved} is not equal to build time in the CSR {(yield dutobj.dut.build_time_csr.status)}"
        assert (yield dutobj.dut.version_csr.status) == SIGN_CONST, f"TMU sign check error: CSR contains {hex((yield dutobj.dut.version_csr.status))} but should contain {hex(SIGN_CONST)}"

        # Check all entries
        bar = tqdm(range(len(simulated_entries)))
        for i in bar:
            checked_index = i
            bar.set_description_str(colored("Checking test: " + simulated_entries_testtype[i], "magenta"))
            yield from check(**simulated_entries[i])

    except AssertionError as err:
        # Print error
        print("AssertionError: " + err.args[0])
        rawbits = (yield dutobj.dut.compressed_fifo.source.raw_bits())
        print("Raw bits of the failing entry: " + bin(rawbits))
        
        # Point to failing entry
        for i in range(len(simulated_entries)):
            if i == checked_index:
                print(colored("---> ", "red"), end="")
            print(simulated_entries_testtype[i] + ": " + hex(simulated_entries[i]["adr"]))
        print(colored("FAILURE.", "red"))
        sys.exit()


def generate_regular_transactions(number : int, waitstated : bool, idle_cycles : int = 0, initial_idle_cycles_override : int = 0, waitstate_cycles_override : int = 0,
    waitstate_counter_boundary : int = 0):
    """Generate regular transactions (non compressed) and queue them for checking

    Args:
        number (int): Number of transactions to simulate
        waitstated (bool): Whether or not the answering SRAM should include waitstates
        idle_cycles (int, optional): The number of idle cycles before the first(!) transaction begins. Defaults to 0.
        initial_idle_cycles_override (int, optional): The maximum number of idle_cycles that can be recorded. Used to test for overflow. Defaults to 0.
        waitstate_cycles_override (int, optional): The number of waitstate cycles to include. If left to 0, but waitstated set to True, the number of waitstates is derived by taking the value of the last 4 bits of the address. Defaults to 0.
        waitstate_counter_boundary (int, optional): The maximum number of waitstate cycles that can be recorded. Used to test for overflow. Requires a larger number in waitstate_cycles_override. Defaults to 0.

    Yields:
        _type_: _description_
    """
    global simulated_entry_count
    first_transaction = True

    for i in range(idle_cycles):
        yield

    adr = rand_diff_num(0)
    for i in range(number):
        adr = rand_diff_num(adr)
        waitstate_counter = 0

        if waitstated:
            adr += MEM_SIZE # Skip first memory, second one is for waitstates
            if waitstate_cycles_override == 0:
                waitstate_counter = adr & 0xF
            else:
                waitstate_counter = waitstate_cycles_override
                yield dutobj.mock_sram.delay_override.eq(waitstate_cycles_override)

        iswrite = rng.randint(0,1)

        if initial_idle_cycles_override != 0 and first_transaction:
            ic = initial_idle_cycles_override
            first_transaction = False
        else:
            ic = idle_cycles

        if waitstate_counter_boundary == 0:
            queue_for_check(adr, iswrite, COMPRESSION.NONE, 0, waitstate_counter, ic, RESULT.SUCCESS, "Regular" + (" waitstated " if waitstated else " ") + "transaction" + (f" (with {idle_cycles} idle cycles)" if idle_cycles > 0 else ""))
        else:
            queue_for_check(adr, iswrite, COMPRESSION.NONE, 0, waitstate_counter_boundary-1, 0, RESULT.SUCCESS, "Waitstate overflow")
            queue_for_check(adr, iswrite, COMPRESSION.NONE, 0, waitstate_cycles_override -  waitstate_counter_boundary - 1, 0, RESULT.SUCCESS, "Waitstate overflow 2/2")

        
        if iswrite:
            yield from dutobj.master_bus.write(adr, 0xABCD, cti=WB_CTI.CLASSIC)
        else:
            yield from dutobj.master_bus.read(adr, cti=WB_CTI.CLASSIC)

    yield dutobj.mock_sram.delay_override.eq(0)
    simulated_entry_count += number


# Generate compressed transactions. Valid for INCREASING_ADDRESS, DECREASING_ADDRESS and SAME_ADDRESS
def generate_compressed_transactions(number : int, typ : COMPRESSION, compression_counter_boundary : int = 0):
    global simulated_entry_count
    adr = rng.randint(1 + dutobj.address_gap * number, MEM_SIZE-1 - dutobj.address_gap * number)
    iswrite = rng.randint(0,1)
    for i in range(number):
        if iswrite:
            yield from dutobj.master_bus.write(adr, 0xABCD, cti=WB_CTI.CLASSIC)
        else:
            yield from dutobj.master_bus.read(adr, cti=WB_CTI.CLASSIC)
        
        if i != number-1:
            if typ == COMPRESSION.INCREASING_ADDRESS:
                adr += dutobj.address_gap
            elif typ == COMPRESSION.DECREASING_ADDRESS:
                adr -= dutobj.address_gap
            else:
                adr = adr
    if compression_counter_boundary == 0:
        queue_for_check(adr, iswrite, typ, number-1, 0, 0, RESULT.SUCCESS, "Compressed Transactions (" + typ.name + ")")
    else:
        queue_for_check(adr - dutobj.address_gap * (number - compression_counter_boundary - 1), iswrite, typ, compression_counter_boundary, 0, 0, RESULT.SUCCESS, "Compressed transaction (compression counter boundary filled)")
        queue_for_check(adr, iswrite, typ, number - compression_counter_boundary - 2, 0, 0, RESULT.SUCCESS, "Compressed transaction (compression counter boundary overstepped)")    # Since the first entry and the second implicitely store one address, cc-2 is used
    
    simulated_entry_count += number


def generate_burst_const_linear(number : int):
    global simulated_entry_count
    adr = rng.randint(1, MEM_SIZE-1)
    iswrite = rng.randint(0,1)

    for i in range(number-1):
        if iswrite:
            yield from dutobj.master_bus.write(adr, 0xFF, cti=WB_CTI.CONST_ADDR_BURST)
        else:
            yield from dutobj.master_bus.read(adr, cti=WB_CTI.CONST_ADDR_BURST)
    
    if iswrite:
        yield from dutobj.master_bus.write(adr, 0xFF, cti=WB_CTI.END_OF_BURST)
    else:
        yield from dutobj.master_bus.read(adr, cti=WB_CTI.END_OF_BURST)

    queue_for_check(adr, iswrite, COMPRESSION.SAME_ADDRESS, number-1, 0, 0, RESULT.SUCCESS, "Constant address, linear burst")
    simulated_entry_count += number


def generate_burst_inc_linear(number : int):
    global simulated_entry_count
    adr = rng.randint(1 + dutobj.address_gap * number, MEM_SIZE-1 - dutobj.address_gap * number)
    iswrite = rng.randint(0,1)
    for i in range(number-1):
        if iswrite:
            yield from dutobj.master_bus.write(adr + i * dutobj.address_gap, 0xFF, cti=WB_CTI.INCREMENT_ADDR_BURST)
        else:
            yield from dutobj.master_bus.read(adr + i * dutobj.address_gap, cti=WB_CTI.INCREMENT_ADDR_BURST)
    
    if iswrite:
        yield from dutobj.master_bus.write(adr + (number-1) * dutobj.address_gap, 0xFF, cti=WB_CTI.END_OF_BURST)
    else:
        yield from dutobj.master_bus.read(adr + (number-1) * dutobj.address_gap, cti=WB_CTI.END_OF_BURST)

    queue_for_check(adr + (number-1) * dutobj.address_gap, iswrite, COMPRESSION.INCREASING_ADDRESS, number-1, 0, 0, RESULT.SUCCESS, "Incremental address, linear burst")
    simulated_entry_count += number


def tb():
    global TEST_SUCCESS
    yield dutobj.dut.recorder.record_csr.storage.eq(1)
    yield

    # Ignore the first empty entry that always occurs
    queue_for_check(0x0, WE.READ, COMPRESSION.SAME_ADDRESS, 0, 0, 0, RESULT.FAILURE, "Initial 0x0 access")
    
    tprint("Regular transactions")
    yield from generate_regular_transactions(40, waitstated=False, initial_idle_cycles_override=2)
    
    tprint("Increasing address transactions")
    yield from generate_compressed_transactions(40, COMPRESSION.INCREASING_ADDRESS)
    
    tprint("Decreasing address transactions")
    yield from generate_compressed_transactions(40, COMPRESSION.DECREASING_ADDRESS)
    
    tprint("Same address transactions")
    yield from generate_compressed_transactions(40, COMPRESSION.SAME_ADDRESS)

    tprint("Regular waitstated transactions")
    yield from generate_regular_transactions(40, waitstated=True)
    
    # TODO: Cannot test right now, because wishbone.SRAM does not support same address bursting, only incremental
    #tprint("Constant address linear burst")
    #yield from generate_burst_const_linear(50)

    tprint("Incremental adress linear burst")
    yield from generate_burst_inc_linear(40)

    tprint("Idle counter")
    yield from generate_regular_transactions(1, waitstated=False, idle_cycles=50)

    tprint("Compression counter overflow")
    yield from generate_compressed_transactions(270, COMPRESSION.INCREASING_ADDRESS, compression_counter_boundary=255)

    tprint("Waitstate counter overflow")
    yield from generate_regular_transactions(1, waitstated=True, waitstate_cycles_override=270, waitstate_counter_boundary=255)

    # Final transaction to confirm the previous one
    yield from dutobj.master_bus.read(0x0)


    # Check that there was enough space to record every entry
    if simulated_entry_count + 1 > FIFO_CAPACITY:
        print(colored("ERROR: The monitor fifo did not have enough capcity to record all simulated entries!!", "red"))
        sys.exit()

    # Check all transactions
    yield from check_all_queued_entries()
    print(colored("SUCCESS.", "green"))
    TEST_SUCCESS = True

def main():
    global TEST_SUCCESS
    global dutobj
    dutobj = DUT(address_mode=ADDRESS_MODE.BYTE_ADDRESSED) # Wishbone is normally word-decoded, just for testing purposes
    run_simulation(dutobj, tb(), vcd_name="ltmu.vcd")
    return TEST_SUCCESS

if __name__ == "__main__":
    main()